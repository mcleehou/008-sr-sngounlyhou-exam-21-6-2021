import { applyMiddleware, createStore } from "redux";
import thunk from 'redux-thunk'
import { toturialReducer } from "../reducer/reducer";

export const store = createStore(toturialReducer,applyMiddleware(thunk))