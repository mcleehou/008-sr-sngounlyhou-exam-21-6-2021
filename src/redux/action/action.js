import { API } from "../../util/api";
import { FETCH_ALL_ARTICLE } from "../constraint";


export const fetchAllTutorial = () => async dp => {
    let response = await API.get('/tutorials');
    return dp({
        type: FETCH_ALL_ARTICLE,
        payload: response.data
    })
}