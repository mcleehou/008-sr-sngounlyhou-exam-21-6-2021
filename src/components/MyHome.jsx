import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllTutorial } from '../redux/action/action'

export default function MyHome() {


    const dispatch = useDispatch()

    const state = useSelector(state => state.tutorial)

    const onload = () => {

        dispatch(fetchAllTutorial())
        console.log(state);
    }
    let user = {
        "title": "string",
        "description": "string"
    }

    return (
        <div>
            <button onClick={onload}>Fetch Data</button>
            <button onClick={() => dispatch({ type: "POST_TUTORIAL", user })}>Post Data</button>
        </div>
    )
}
