import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import MyHome from './components/MyHome'

export default function App() {
    return (
        <div>
           <Router>
                <Route exact path="/" component={MyHome}/>   
            </Router> 
        </div>
    )
}
